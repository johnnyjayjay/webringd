/*
 * webringd
 * Copyright (c) 2023 JohnnyJayJay
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-FileCopyrightText: 2023 JohnnyJayJay
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
#define _GNU_SOURCE
#include <ctype.h>
#include <err.h>
#include <errno.h>
#include <fcntl.h>
#include <getopt.h>
#include <microhttpd.h>
#include <pthread.h>
#include <regex.h>
#include <signal.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <time.h>
#include <unistd.h>
#include <uthash.h>
#include <utlist.h>

#define INDEX_ERROR                                                     \
    "Could not load the index page. Contact the server administrator."
#define NOT_FOUND_MSG "Page not found"
#define REDIRECT_CODE 302
#define STATIC_RESPONSE(str, code)                                  \
    ((struct response){.response = MHD_create_response_from_buffer( \
        strlen(str), str, MHD_RESPMEM_PERSISTENT),                  \
     .status_code = code})
#define NOT_FOUND                                                   \
    html_response(404, "Page Not Found",                            \
                  "The page you are looking for does not exist.")
#define CSV_SEP ",\n"
#define CONTENT_TYPE(resp, text) MHD_add_response_header(resp.response, "content-type", text)

regex_t url_regex;
regex_t id_regex;

struct config {
    char *new_page_program;
    char *index_file;
    char *ring_file;
    char *context_url;
    char *username;
    char *password;
    unsigned int max_url_length;
    unsigned int queue_length;
    unsigned int max_id_length;
    bool auto_admit;
};

// a page in the webring
struct page {
    const char *id;
    const char *url;
    UT_hash_handle hh;
    struct page *prev;
    struct page *next;
};

struct state {
    struct MHD_Daemon *server;
    // pointer to the hashtable head of the ring
    struct page *page_table;
    // pointer to the doubly linked list head of the ring
    struct page *page_ring;
    // pages pending approval
    struct page *pending_pages;
    pthread_rwlock_t page_lock;
    const struct config config;
};

int read_csv(FILE *input, char **line, size_t *line_size, unsigned int cols,
             char **result) {
    ssize_t ret = getline(line, line_size, input);
    if (ret == -1) {
        return -1;
    }
    char *saveptr;
    result[0] = strtok_r(*line, CSV_SEP, &saveptr);
    for (unsigned int i = 1; i < cols; i++) {
        char *token = strtok_r(NULL, CSV_SEP, &saveptr);
        if (token == NULL) {
            return -1;
        }
        result[i] = token;
    }
    return 0;
}

bool add_page(struct state *state, const char *page_id, const char *url) {
    struct page *existing_page;
    HASH_FIND_STR(state->page_table, page_id, existing_page);
    if (existing_page != NULL) {
        return false;
    }
    struct page *new_entry = malloc(sizeof(struct page));
    new_entry->id = strdup(page_id);
    new_entry->url = strdup(url);
    CDL_APPEND(state->page_ring, new_entry);
    HASH_ADD_STR(state->page_table, id, new_entry);
    return true;
}

// not thread safe
void read_ring(struct state *state) {
    FILE *file = fopen(state->config.ring_file, "r");
    if (file == NULL) {
        return;
    }
    char *csv[2];
    char *line = NULL;
    size_t line_size = 0;
    while (read_csv(file, &line, &line_size, 2, csv) == 0) {
        add_page(state, csv[0], csv[1]);
    }
    free(line);
    fclose(file);
}

int write_pages(int fd, struct page *hash) {
    int size = 0;
    struct page *page;
    CDL_FOREACH(hash, page) {
        size += dprintf(fd, "%s,%s\n", page->id, page->url);
    }
    return size;
}

// not thread safe
void dump_ring(struct state *state) {
    int fd = open(state->config.ring_file, O_CREAT | O_WRONLY, S_IRUSR | S_IRGRP | S_IROTH | S_IWUSR);
    if (fd == -1) {
        err(2, "Webring data could not be saved");
    }
    write_pages(fd, state->page_table);
    close(fd);
}

void free_page(struct page *page) {
    free((void *)page->id);
    free((void *)page->url);
    free(page);
}

bool persist_page(struct state *state, const char *page_id, const char *url) {
    pthread_rwlock_wrlock(&state->page_lock);
    bool result = add_page(state, page_id, url);
    if (result) {
        dump_ring(state);
    }
    pthread_rwlock_unlock(&state->page_lock);
    return result;
}

bool delete_page(struct state *state, const char *page_id) {
    pthread_rwlock_wrlock(&state->page_lock);
    struct page *ring_target, *pending_target;
    HASH_FIND_STR(state->page_table, page_id, ring_target);
    HASH_FIND_STR(state->pending_pages, page_id, pending_target);
    if (pending_target != NULL) {
        HASH_DEL(state->pending_pages, pending_target);
        free_page(pending_target);
    }
    if (ring_target == NULL) {
        pthread_rwlock_unlock(&state->page_lock);
        return false;
    }
    HASH_DEL(state->page_table, ring_target);
    CDL_DELETE(state->page_ring, ring_target);
    free_page(ring_target);
    dump_ring(state);
    pthread_rwlock_unlock(&state->page_lock);
    return true;
}

bool approve_page(struct state *state, const char *page_id) {
    pthread_rwlock_wrlock(&state->page_lock);
    struct page *ring_target, *pending_target;
    bool result = false;
    HASH_FIND_STR(state->page_table, page_id, ring_target);
    HASH_FIND_STR(state->pending_pages, page_id, pending_target);
    if (pending_target != NULL) {
        if (ring_target == NULL) {
            add_page(state, pending_target->id, pending_target->url);
            dump_ring(state);
        }
        HASH_DEL(state->pending_pages, pending_target);
        free_page(pending_target);
        result = true;
    }
    pthread_rwlock_unlock(&state->page_lock);
    return result;
}

void clear_pending(struct state *state, bool approve) {
    pthread_rwlock_wrlock(&state->page_lock);
    struct page *page, *tmp;
    HASH_ITER(hh, state->pending_pages, page, tmp) {
        HASH_DEL(state->pending_pages, page);
        if (approve) {
            add_page(state, page->id, page->url);
        } else {
            struct page *existing;
            HASH_FIND_STR(state->page_table, page->id, existing);
            if (existing != NULL) {
                HASH_DEL(state->page_table, existing);
                free_page(existing);
            }
        }
        free_page(page);
    }
    if (approve) {
        dump_ring(state);
    }
    pthread_rwlock_unlock(&state->page_lock);
}

const struct page *find_page(struct state *state, const char *page_id) {
    pthread_rwlock_rdlock(&state->page_lock);
    struct page *result;
    HASH_FIND_STR(state->page_table, page_id, result);
    pthread_rwlock_unlock(&state->page_lock);
    return result;
}

bool page_is_invalid(const struct config *cfg, const char *page_id,
                     const char *url) {
    return strlen(page_id) > cfg->max_id_length ||
        strlen(url) > cfg->max_url_length ||
        regexec(&id_regex, page_id, 0, NULL, 0) ||
        regexec(&url_regex, url, 0, NULL, 0);
}

bool page_is_pending(struct state *state, const char *page_id) {
    struct page *page;
    HASH_FIND_STR(state->pending_pages, page_id, page);
    return page != NULL;
}

#define HASH_FREE(head, free_fn, el, tmp)       \
    HASH_ITER(hh, head, el, tmp) {              \
        HASH_DEL(head, el);                     \
        free_fn(el);                            \
    }

// handlers and responses

struct response {
    struct MHD_Response *response;
    int status_code;
};

struct response html_response(int code, const char *title, const char *body_fmt,
                              ...) {
    va_list ap;
    va_start(ap, body_fmt);
    char *body = NULL;
    vasprintf(&body, body_fmt, ap);

    char *html = NULL;
    int len =
        asprintf(&html,
                 "<html lang=\"en\"><head><link rel=\"icon\" "
                 "href=\"data:,\"><title>%1$s</title></head><body><h1>%1$s</h1> "
                 "%2$s</body>",
                 title, body);
    free(body);
    struct response resp = {.response = MHD_create_response_from_buffer(
        len, html, MHD_RESPMEM_MUST_FREE),
    .status_code = code};
    CONTENT_TYPE(resp, "text/html");
    return resp;
}

struct response redirect_response(const char *url) {
    struct MHD_Response *resp = MHD_create_response_from_buffer(0, "", MHD_RESPMEM_PERSISTENT);
    MHD_add_response_header(resp, "location", url);
    return (struct response) {.response = resp, .status_code = 302};
}

struct response page_csv_response(struct page *hash) {
    int fd = fileno(tmpfile());
    int size = write_pages(fd, hash);
    struct response resp = {.response = MHD_create_response_from_fd(size, fd), .status_code = 200};
    CONTENT_TYPE(resp, "text/csv");
    return resp;
}

// --------------
// Table building
// --------------

#define NAME_URL "<td>%1$s</td><td><a href=\"%2$s\">%2$s</a></td>"
#define REMOVE "<a href=\"/admin/remove?id=%1$s\">Remove</a>"
#define APPROVE "<a href=\"/admin/approve?id=%1$s\">Approve</a>"
#define DISMISS "<a href=\"/admin/remove?id=%1$s\">Dismiss</a>"

int page_table_row(char **result, __attribute__((unused)) struct state *state,
                   struct page *page) {
    return asprintf(result, "<tr>" NAME_URL "</tr>", page->id, page->url);
}

int admin_table_pending_row(char **result, struct state *state,
                            struct page *page) {
    char *page_state, *actions;
    if (state->config.auto_admit) {
        page_state = "Live (<b>New</b)";
        asprintf(&actions, REMOVE " / " APPROVE, page->id);
    } else {
        page_state = "Pending";
        asprintf(&actions, DISMISS " / " APPROVE, page->id);
    }
    int len = asprintf(result, "<tr>" NAME_URL "<td>%3$s</td><td>%4$s</td></tr>",
                       page->id, page->url, page_state, actions);
    free(actions);
    return len;
}

int admin_table_row(char **result, struct state *state, struct page *page) {
    if (page_is_pending(state, page->id)) {
        *result = strdup("");
        return 0;
    } else {
        return asprintf(result, "<tr>" NAME_URL "<td>Live</td><td>" REMOVE "</td>",
                        page->id, page->url);
    }
}

char *table_rows(struct state *state, struct page *hash,
                 int (*create_row)(char **, struct state *, struct page *)) {
    char *result = malloc(1);
    size_t len = 0;
    struct page *page;
    // iterate backwards through the list
    struct page *head = hash == NULL ? NULL : hash->prev;
    CDL_FOREACH2(head, page, prev) {
        char *row = NULL;
        int row_len = create_row(&row, state, page);
        result = realloc(result, 1 + len + row_len);
        memcpy(&result[len], row, row_len);
        len += row_len;
        free(row);
    }
    result[len] = '\0';
    return result;
}

char *build_page_table(struct state *state) {
    char *table;
    char *rows = table_rows(state, state->page_table, &page_table_row);
    asprintf(&table, "<table><tr><th>Name</th><th>URL</th></tr>%s</table>", rows);
    free(rows);
    return table;
}

char *build_admin_table(struct state *state) {
    char *table;
    char *new_rows =
        table_rows(state, state->pending_pages, &admin_table_pending_row);
    char *old_rows = table_rows(state, state->page_table, &admin_table_row);
    asprintf(&table,
             "<table><tr><th>Name</th><th>URL</th><th>State</th><th>Actions</"
             "th></tr>%s%s</table>",
             new_rows, old_rows);
    free(new_rows);
    free(old_rows);
    return table;
}

char *trimmed(char *str) {
    while (*str && isspace(*str)) {
        str++;
    }
    return str;
}

// maybe use a cached temp file for index page instead of rebuilding table every time.
// invalidate index page cache when ring is modified. use stat of temp file to get size
// function to use: mkstemp(3)
int generate_index_page(struct state *state) {
    FILE *template = fopen(state->config.index_file, "r");
    if (template == NULL) {
        return -1;
    }
    FILE *file = tmpfile();
    char *line = NULL;
    size_t n;
    while (getline(&line, &n, template) != -1) {
        if (strcmp("{{}}\n", trimmed(line)) == 0) {
            char *table = build_page_table(state);
            fputs(table, file);
            fputc('\n', file);
            free(table);
        } else {
            fputs(line, file);
        }
    }
    free(line);
    fseek(file, 0, SEEK_SET);
    fclose(template);
    return fileno(file);
}

struct response index_page(struct state *state) {
    int fd = generate_index_page(state);
    struct stat st;
    if (fd == -1 || fstat(fd, &st) == -1) {
        fprintf(stderr, "Error loading index page '%s': %s\n",
                state->config.index_file, strerror(errno));
        return STATIC_RESPONSE(INDEX_ERROR, 500);
    }

    return (struct response){.response =
    MHD_create_response_from_fd(st.st_size, fd),
    .status_code = 200};
}

struct response admin_page(struct state *state) {
    pthread_rwlock_rdlock(&state->page_lock);
    char *table = build_admin_table(state);
    struct response resp = html_response(
        200, "Webring Control",
        "Welcome to the admin dashboard of your webring"
        "<h2>Add a page</h2>"
        "<form action=\"/admin/add\" method=\"get\">"
        "<label>ID: <input name=\"id\" type=\"text\" /></label> "
        "<label>URL: <input name=\"url\" type=\"url\" /></label>"
        "<button type=\"submit\">Add</button>"
        "</form>"
        "<h2>Pages</h2>"
        "<p>In the table below you can see pages that are already part of the "
        "webring and pages that want to join, ordered from newest to oldest.</p>"
        "<p>You can either <strong><a id=\"approve-all\" "
        "href=\"/admin/approve-all\">approve all</a></strong> or <strong><a "
        "id=\"dismiss-all\" href=\"/admin/dismiss-all\">dimiss all</a></strong> "
        "pending/new pages at once. Alternatively, you can approve/dismiss each "
        "page individually.</p><p><b>Page queue</b>: %d/%d (when full, you have to "
        "approve/dismiss pending/new pages so new pages can join)</p>"
        "%s",
        HASH_COUNT(state->pending_pages), state->config.queue_length, table);
    free(table);
    pthread_rwlock_unlock(&state->page_lock);
    return resp;
}

struct response member_page(struct state *state, const char *page_id) {
    const struct page *target = find_page(state, page_id);
    return target == NULL ? NOT_FOUND : redirect_response(target->url);
}

void notify_new_page(const char *program, const char *page_id,
                     const char *url) {
    if (program == NULL) {
        return;
    }

    pid_t pid = fork();
    if (pid == -1) {
        execl(program, program, page_id, url, (char *)NULL);
    }
}

bool request_queue_full(struct state *state) {
    pthread_rwlock_rdlock(&state->page_lock);
    bool full = HASH_COUNT(state->pending_pages) >= state->config.queue_length;
    pthread_rwlock_unlock(&state->page_lock);
    return full;
}

struct response ring_walk(struct MHD_Connection *conn, struct state *state,
                          const char *page_id, const char *action) {
    int is_next = strcasecmp(action, "next") == 0;
    int is_prev = strcasecmp(action, "prev") == 0;
    if (!is_next && !is_prev) {
        return NOT_FOUND;
    }

    const struct page *source = find_page(state, page_id);
    if (source == NULL) {
        // New page entry/request
        const char *source_url =
            MHD_lookup_connection_value(conn, MHD_HEADER_KIND, "referer");
        // if source_url can be found, it is duplicated in url to allow further
        // sanitisation
        char *url;
        if (source_url == NULL) {
            // no referer header given, look in "source" parameter instead
            source_url =
                MHD_lookup_connection_value(conn, MHD_GET_ARGUMENT_KIND, "source");
            if (source_url == NULL) {
                return STATIC_RESPONSE(
                    "To register for the webring, your website must be either "
                    "configured to allow the 'referer' header "
                    "OR the URL of your website must be given as a query "
                    "parameter 'source' in the webring URL.",
                    400);
            }
            // if source found in parameter, do URL decode
            url = strdup(source_url);
            MHD_http_unescape(url);
        } else {
            url = strdup(source_url);
        }

        // If present, strip query parameters from URL by excluding everything
        // following '?'
        char *param_section_start = strstr(url, "?");
        if (param_section_start != NULL) {
            *param_section_start = '\0';
        }

        struct response response;
        if (page_is_invalid(&state->config, page_id, url)) {
            // Page request is invalid in some way.
            response = html_response(
                400, "Webring request failed",
                "Your page could not be submitted to the ring. "
                "Make sure that you respected the following: <ul>"
                "<li>The page ID is not longer than %d characters</li>"
                "<li>The page ID only consists of alphanumeric characters, "
                "dashes and underscores</li>"
                "<li>The URL is not longer than %d characters</li>"
                "<li>The URL is valid and does not contain strange "
                "characters</li>"
                "</ul>If you can't find the issue, contact the webmaster for "
                "help.",
                state->config.max_id_length, state->config.max_url_length);
        } else if (request_queue_full(state)) {
            // Request queue is full - must be cleared by the webmaster before
            // request can be submitted
            response = html_response(
                503, "Webring request queue full",
                "Your request to be added to the webring unfortunately cannot "
                "be submitted at this time because there are many other "
                "requests already waiting for approval. This is a security "
                "measure to avoid request spam. Check back later when the "
                "webmaster has cleared the queue! Thank you.");
        } else if (state->config.auto_admit) {
            // page can be automatically added to ring
            if (persist_page(state, page_id, url)) {
                notify_new_page(state->config.new_page_program, page_id, url);
            }
            response = ring_walk(conn, state, page_id, action);
        } else if (page_is_pending(state, page_id)) {
            // There already is a pending request for a webring addition using
            // the same page id
            response =
                html_response(409, "Webring request already pending",
                              "Your request to be added to the webring was not "
                              "submitted because there already is a page named "
                              "'%s' waiting for approval.<p>If it's yours, "
                              "please wait until the webmaster approves your "
                              "request, otherwise pick a different name.</p>",
                              page_id);
        } else {
            // page request is submitted
            notify_new_page(state->config.new_page_program, page_id, url);
            response = html_response(200, "Webring request sent",
                                     "You've sent a request to be added to the "
                                     "webring. The webmaster will review it!");
        }
        free(url);
        return response;
    } else {
        return redirect_response(is_next ? source->next->url
                                         : source->prev->url);
    }
}

struct response admin_handler(struct MHD_Connection *conn, struct state *state,
                              const char *action) {
    const char *page_id =
        MHD_lookup_connection_value(conn, MHD_GET_ARGUMENT_KIND, "id");
    if (action == NULL) {
        return admin_page(state);
    } else if (strcmp(action, "pending") == 0) {
        return page_csv_response(state->pending_pages);
    } else if (strcmp(action, "approve-all") == 0) {
        clear_pending(state, true);
    } else if (strcmp(action, "dismiss-all") == 0) {
        clear_pending(state, false);
    } else if (strcmp(action, "add") == 0) {
        page_id = MHD_lookup_connection_value(conn, MHD_GET_ARGUMENT_KIND, "id");
        const char *enc_url =
            MHD_lookup_connection_value(conn, MHD_GET_ARGUMENT_KIND, "url");
        if (page_id == NULL || enc_url == NULL) {
            return STATIC_RESPONSE("Missing form data", 400);
        }

        char *url = strdup(enc_url);
        MHD_http_unescape(url);
        struct response resp;
        if (page_is_invalid(&state->config, page_id, url)) {
            resp = html_response(
                400, "Could not add page",
                "The page could not be added. Make sure the ID and URL respect "
                "length and format constraints.");
        } else if (!persist_page(state, page_id, url)) {
            resp = html_response(
                409, "Page already exists",
                "There already is a page by the name of <i>%s</i> in the ring."
                "You must remove it before adding a page with the same name.",
                page_id);
        } else {
            resp = redirect_response("/admin");
        }
        free(url);
        return resp;
    } else if (page_id == NULL) {
        return STATIC_RESPONSE("Page ID parameter missing", 400);
    } else if (strcmp(action, "approve") == 0) {
        if (!approve_page(state, page_id)) {
            return NOT_FOUND;
        }

    } else if (strcmp(action, "remove") == 0) {
        delete_page(state, page_id);
    } else {
        return NOT_FOUND;
    }
    return redirect_response("/admin");
}

// TODO: prepend context URL to HTML anchors
enum MHD_Result handler(void *cls, struct MHD_Connection *conn, const char *url,
                        __attribute__((unused)) const char *method,
                        __attribute__((unused)) const char *version,
                        __attribute__((unused)) const char *upload_data,
                        __attribute__((unused)) size_t *upload_data_size,
                        __attribute__((unused)) void **con_cls) {
    struct state *state = (struct state *)cls;
    struct response resp;

    char *url_cpy = strdup(url);
    char *saveptr;

    const char *page_id = strtok_r(url_cpy + 1, "/", &saveptr);
    const char *action = strtok_r(NULL, "/", &saveptr);
    /*printf(
        "Received request! URL=%s, Method=%s, Version=%s\nID: %s, action: %s\n",
        url, method, version, page_id, action);*/
    if (strtok_r(NULL, "/", &saveptr) != NULL) {
        // URL = /page_id/action/something_else
        // there should only be 2 path elements, but we've found a third
        resp = NOT_FOUND;
    } else if (page_id == NULL) {
        // URL = /
        resp = state->config.index_file ? index_page(state) : NOT_FOUND;
    } else if (strcmp(page_id, "pages") == 0) {
        resp = page_csv_response(state->page_table);
    } else if (strcmp(page_id, "admin") == 0) {
        if (!state->config.password) {
            resp = html_response(404, "Admin dashboard is disabled",
                                 "The admin dashboard is not available because "
                                 "no admin password was set.");
        } else {
            char *password;
            char *username = MHD_basic_auth_get_username_password(conn, &password);
            if (username == NULL || password == NULL ||
                strcmp(username, state->config.username) != 0 ||
                strcmp(password, state->config.password) != 0) {
                MHD_free(username);
                MHD_free(password);
                free(url_cpy);
                struct MHD_Response *response = MHD_create_response_from_buffer(0, "", MHD_RESPMEM_PERSISTENT);
                int ret = MHD_queue_basic_auth_fail_response(conn, "webring", response);
                MHD_destroy_response(response);
                return ret;
            }
            MHD_free(username);
            MHD_free(password);
            resp = admin_handler(conn, state, action);
        }
    } else if (action == NULL) {
        // URL = /page_id
        resp = member_page(state, page_id);
    } else {
        // URL = /page_id/action
        resp = ring_walk(conn, state, page_id, action);
    }

    free(url_cpy);
    MHD_add_response_header(resp.response, "content-security-policy", "script-src 'none'");
    int ret = MHD_queue_response(conn, resp.status_code, resp.response);
    MHD_destroy_response(resp.response);
    return ret;
}

// TODO: implement IP-based rate-limiting using set of IP addresses that are
// currently blocked from adding

int main(int argc, char **argv) {
    uint16_t port = 8080;
    unsigned int num_threads = 1;

    static struct config cfg = {
    .index_file = NULL,
    .ring_file = "ring.csv",
    .auto_admit = false,
    .context_url = "/",
    .username = "admin",
    .password = NULL,
    .new_page_program = NULL,
    .max_id_length = 20,
    .max_url_length = 500,
    .queue_length = 25};

    static struct option options[] = {
    {"index", required_argument, 0, 'i'},
    {"file", required_argument, 0, 'f'},
    {"port", required_argument, 0, 'P'},
    {"threads", required_argument, 0, 't'},
    {"auto-admit", no_argument, 0, 0},
    {"max-id-length", required_argument, 0, 1},
    {"max-url-length", required_argument, 0, 2},
    {"queue", required_argument, 0, 'q'},
    {"new-page-hook", required_argument, 0, 'n'},
    {"username", required_argument, 0, 'u'},
    {"password", required_argument, 0, 'p'},
    {0}};

    int opt;
    int option_index = 0;
    while ((opt = getopt_long(argc, argv, "i:f:P:t:q:n:u:p:", options, &option_index)) != -1) {
        switch (opt) {
            case 0:
                cfg.auto_admit = true;
                break;
            case 1:
                cfg.max_id_length = atoi(optarg);
                break;
            case 2:
                cfg.max_url_length = atoi(optarg);
                break;
            case 'i':
                cfg.index_file = optarg;
                break;
            case 'f':
                cfg.ring_file = optarg;
                break;
            case 'P':
                port = atoi(optarg);
                break;
            case 't':
                num_threads = atoi(optarg);
                break;
            case 'q':
                cfg.queue_length = atoi(optarg);
                break;
            case 'n':
                cfg.new_page_program = optarg;
                break;
            case 'u':
                cfg.username = optarg;
                break;
            case 'p':
                cfg.password = optarg;
                break;
            case '?':
                return 1;
            default:
                fprintf(
                    stderr,
                    "Detected an option (%d) that is specified, but was not "
                    "implemented correctly. This is a bug, please report it!\n",
                    opt);
        }
    }

    if (!cfg.password) {
        fprintf(stderr, "No admin password set – admin dashboard is disabled.\n");
    }

    if (!cfg.index_file) {
        fprintf(stderr, "No index file set. Accesses to the web root will result in a 404.\n");
    } else {
        FILE *f = fopen(cfg.index_file, "r");
        if (f == NULL) {
            err(1, "could not open index file");
        } else {
            fclose(f);
        }
    }

    struct state state = {
    .config = cfg,
    .page_ring = NULL,
    .page_table = NULL,
    .pending_pages = NULL};

    regcomp(&url_regex, "^https?://\\w+\\.\\w+(\\.\\w+)*[^,<>\"'&?]*$",
            REG_EXTENDED | REG_NOSUB | REG_NEWLINE);
    regcomp(&id_regex, "^[a-zA-Z0-9_\\-]", REG_NOSUB | REG_NEWLINE);

    read_ring(&state);

    // ignore SIGCHILD - this prevents subprocesses from being turned into
    // zombies
    signal(SIGCHLD, SIG_IGN);

    struct MHD_Daemon *server = MHD_start_daemon(
        MHD_USE_ERROR_LOG | MHD_USE_DUAL_STACK | MHD_USE_INTERNAL_POLLING_THREAD,
        port, NULL, NULL, &handler, &state, MHD_OPTION_THREAD_POOL_SIZE,
        num_threads, MHD_OPTION_END);

    state.server = server;
    if (server == NULL) {
        errx(1, "Could not start web server.");
    }

    if (pthread_rwlock_init(&state.page_lock, NULL)) {
        errx(1, "Could not initialise a lock");
    }

    fprintf(stderr, "Sever started, listening on port %d\n", port);

    while (1) {
        pause();
    }

    MHD_stop_daemon(server);
    regfree(&url_regex);
    regfree(&id_regex);
    struct page *el, *tmp;
    HASH_FREE(state.page_table, free_page, el, tmp);
    return 0;
}
