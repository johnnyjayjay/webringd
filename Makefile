# SPDX-License-Identifier: AGPL-3.0-or-later
# SPDX-FileCopyrightText: 2023 JohnnyJayJay
CFLAGS = -Wall -Wextra -std=gnu17 -pthread -Ilibmicrohttpd/src/include -Iuthash/src
LDFLAGS = -pthread
LDLIBS =
release-output = webringd
debug-output = webringd-dbg
sanitizers = address undefined leak #thread
sanitize-opts = $(foreach type,$(sanitizers),-fsanitize=$(type))

release: $(release-output)
$(release-output): webringd.c libmicrohttpd.a
	$(CC) $(CFLAGS) $(LDFLAGS) $(LDLIBS) -O3 -o $(release-output) $^

debug: CFLAGS += -Og $(sanitize-opts) -g
debug: LDFLAGS += $(sanitize-opts)
debug: $(debug-output)

$(debug-output): webringd.o libmicrohttpd.a
	$(CC) $(LDLIBS) $(LDFLAGS) -o $(debug-output) $^

libmicrohttpd.a:
	cd libmicrohttpd && autoreconf -fi
	cd libmicrohttpd && ./configure --with-threads=posix --disable-postprocessor --disable-dauth --disable-httpupgrade --disable-https
	$(MAKE) -C libmicrohttpd
	cp libmicrohttpd/src/microhttpd/.libs/libmicrohttpd.a .

.PHONY: clean
clean:
	rm -f *.o libmicrohttpd.a $(debug-output) $(release-output)
