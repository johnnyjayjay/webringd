# webringd

A super tiny [webring](https://leftfold.tech/pages/webring) server to satisfy your early web nostalgia.

**NOTE:** this is not guaranteed to be stable or 100% functional yet. If you encounter issues, feel free to open an issue.

## Features

- open registrations or registrations that require approval
- registration via hyperlink
- customisable index file
- admin dashboard (`/admin`)
- ways to hook into and manage the webring outside of the browser
- many [configuration options](#configuration)
- very small (420kB), low memory profile

## Registrations

Websites can register by simply linking to `/{id}/next` or `/{id}/prev` (where `{id}` is the page ID the website would like to use) and clicking on one of those links. Websites that suppress the `referer` header must also add their (URL-encoded) URL as a parameter to the link, e.g. `/{id}/next?url=http%3A%2F%2Fexample.com`.

When websites register, they're put in a queue. When this queue is full, no more websites can register. In order for new websites to be able to join, pages must be removed from this queue by either approving or dimissing/deleting them. The purpose of this is spam prevention.

Additionally, there are two different registration policies:

- Manual approval (default) means that pages in the queue will not be put in the ring until they are explicitly approved.
- Automatic admission (enabled via `--auto-admit`) means that pages *will* be put in the ring automatically upon registration, but that they will *also* be put in the queue and need to be approved to make room for more registrations.

## New page hook

You can specify a program that will be run each time a new page registers/sends a request. The page ID and URL will be the first two arguments. This can be used to set up a system that notifies you when new pages join so you can check them. For example, you could write a file `send-webhook` like this:

``` sh
#!/bin/bash
curl \
  -H "Accept: application/json" \
  -H "Content-Type: application/json" \
  -X POST \
  --data "{\"content\": \"New page: `$1` ($2)\"}" \
  <webhook-link>
```

Where `<webhook-link>` is the URL target of some webhook in an instant messaging service like Discord. If you now start webringd with `--new-page-hook send-webhook`, you'll get a Discord message for every page that registers. 

## API

While the webring can be managed in the browser via the `/admin` page, you can also do this externally via the HTTP API:

- GET `/pages`: returns CSV (`id,url`) of all current (active) pages in the ring.
- GET `/admin/queue`: returns CSV (`id,url`) of all pages that are currently pending or that are members but have not been approved yet.
- GET `/admin/approve?id=...`: Remove the page with the given ID from the registration queue and add it to the ring if it has not been added automatically.
- GET `/admin/remove?id=...`: Remove the page with the given ID from the registration queue and the ring, if applicable.
- GET `/admin/add?id=...&url=...`: Add a page to the ring, skipping the registration queue. `id` and `url` are passed as URL encoded query parameters.
  - this is GET rather than POST mainly because parsing a form body is kind of a pain in the ass
- GET `/admin/approve-all`: Approve all pages in the registration queue.
- GET `/admin/dismiss-all`: Dismiss all pages in the registration queue. This also removes them from the ring, in case they're already part of it. 

## Install

The software is made for GNU/Linux systems. It uses GNU extensions on top of standard C, meaning that this will probably not work on other systems out of the box.

You can download the [latest release](https://codeberg.org/johnnyjayjay/webringd/releases) from this page or you can clone and build the repository yourself:

``` sh
git clone --recurse-submodules https://codeberg.org/johnnyjayjay/webringd.git
cd webringd
make
```

Then, simply run it using `./webringd`, for example like so:

``` sh
./webringd --port 1234 --password very-secure --index index.html
```

## Configuration

Config options are passed via the command line.

| Option                    | Description                                                                                                                                                                                                       | Default                      |
|:--------------------------|:------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|:-----------------------------|
| `-f`, `--file <file>`     | CSV file storing the webring members. Doesn't have to exist already.                                                                                                                                              | `ring.csv`                   |
| `-P`, `--port <num>`      | Port number                                                                                                                                                                                                       | `8080`                       |
| `-t`, `--threads <num>`   | Number of threads to use for server I/O                                                                                                                                                                           | `1`                          |
| `--auto-admit`            | Whether users can add sites to the webring themselves, without waiting for approval first                                                                                                                         | Not given                    |
| `--max-id-length <num>`   | Maximum length for a page id                                                                                                                                                                                      | `20`                         |
| `--max-url-length <num>`  | Maximum length for a page URL                                                                                                                                                                                     | `200`                        |
| `-q`, `--queue <num>`     | Maximum number of pending page requests/maximum number of added pages without supervision                                                                                                                         | `25`                         |
| `-n`, `--new-page-hook`   | Program that will be called when a new page is added to the ring (or a page addition is requested). Program will receive the page ID and URL as arguments.                                                        | /                            |
| `-i`, `--index <file>`    | Template for the index file of the server. Any line consisting of the string (potentially with leading whitespace) `{{}}` will be replaced by an HTML table containing the ring members. Line endings must be LF. | No index page                |
| `-u`, `--username <name>` | Admin username                                                                                                                                                                                                    | `admin`                      |
| `-p`, `--password <pw>`   | Admin password                                                                                                                                                                                                    | Not set; admin page disabled |

## Development

You can hack on this yourself. Here's what I use to get hot reloading:

``` sh
ls *.c | entr -sr 'make debug && ./webringd-dbg'
```

## License

Copyright (C) 2023 JohnnyJayJay

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License,
or (at your option) any later version.

When hosting your webring using (modified versions of) this software, remember from the license that:

> If your software can interact with users remotely through a computer network, you should also make sure that it provides a way for users to get its source. For example, if your program is a web application, its interface could display a "Source" link that leads users to an archive of the code. There are many ways you could offer source, and different solutions will be better for different programs; see section 13 for the specific requirements.

If you use an unmodified version of this software, a link to this repository on your index page is sufficient.
